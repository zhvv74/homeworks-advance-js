//Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

/* Уместность использования конструкции try-catch может быть оправдана в случаях, например
при работе с сервером - когда неизвестно, как корректно пользователь может ввести какую-любо
информацию. Еще случай работы в команде, когда любой из участников проекта может ошибочно или
невнимательно ввести код или блок кода. Тогда, это нужно учесть и поместить такой код в "ловушку",
где подобные ошибки будут отлавливаться и не пускать работу программы дальше до тех пор, пока
не введется корректный код.
 */

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function createList(array, parent) {
    const ul = document.createElement("ul");
    document.querySelector(parent).append(ul);
    array.forEach(function (item) {
        try {
            if (!item.hasOwnProperty("author")) {
                throw new Error("no property 'author'");
            } else if(!item.hasOwnProperty("name")){
                throw new Error("no property 'name'");
            } else if(!item.hasOwnProperty("price")){
                throw new Error("no property 'price'");
            } else {
                const li = document.createElement("li");
                li.innerText = `${item.name} ${item.author} ${item.price}`
                ul.append(li);
            }
        } catch (error) {
console.error(error);
        }
    })
}

createList(books, "#root");