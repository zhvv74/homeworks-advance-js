/*Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript.
Прототипное наследование - это прежде всего оптимизация написания кода. Т. е. любые объекты
наследуются друг от друга, имея начальные общие свойства, но добавляя свои. Поэтому, чтобы
каждый раз не повторять одинаковые свойства, тем самым усложняя код - они берутся от класса - предка
и затем добавляются своими.
*/

class Employee {
    constructor (name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name(){
        return this._name;
    }
    get age(){
        return this._age;
    }
    get salary(){
        return this._salary;
    }
    set name(value){
        this._name = value;
    }
    set age(value){
        this._age = value;
    }
    set salary(value){
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get lang(){
        return this._lang;
    }
    set lang(value){
        this._lang = value;
    }
    get salary(){
        return super.salary;
    }
    set salary(value){
        super.salary = value * 3;
    }
}

const programmer1 = new Programmer("Tolik",30, 2000, "english");
const programmer2 = new Programmer("Sveta",18, 1000, "ukranian");
const programmer3 = new Programmer("Grisha",52, 2020, "china");

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);